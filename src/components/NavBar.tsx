import React from 'react';
import { Link, withRouter } from 'react-router-dom';
import { useAuth0 } from '../contexts/auth0-context';

function Navbar() {
    const { isLoading, user, loginWithRedirect, logout, isAuthenticated } = useAuth0();

    return (
        <header>
            <div className="container-fluid position-relative no-side-padding">

                <div className="menu-nav-icon" data-nav-menu="#main-menu">
                    <i className="ion-navicon" />
                </div>

                <ul className="main-menu visible-on-click" id="main-menu">
                   
                    <li>
                    <Link className={"nav-link"} to={"/"}>
                        {!isLoading && !user && (
                            <>
                                <button className="btn btn-dark" onClick={loginWithRedirect}>
                                    Sign In
                                </button>
                            </>
                        )}

                        {!isLoading && user && (
                            <>
                                <div>
                                    <label className="mr-2">{user.name}</label>
                                    <button className="btn btn-dark" onClick={() => logout({ returnTo: window.location.origin })}>
                                        Sign Out 
                                    </button>
                                </div>
                            </>
                        )}
                    </Link>
                    </li>
                    {isAuthenticated && (
                    <li><Link className={"nav-link"} to={"/todos"}> Home </Link></li>)}
                    {isAuthenticated && (
                    <li><Link className={"nav-link"} to={"/todos/create"}> Create Todo</Link></li>
                    )}
                </ul>
            </div>
        </header>
    );
}

export default withRouter(Navbar);