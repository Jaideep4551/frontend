import React, { useState, useEffect } from 'react';
import { Link, Route, useHistory, useParams } from 'react-router-dom';
import { useAuth0 } from '../contexts/auth0-context';


function Home():JSX.Element {
  let history = useHistory()
  let  TodosId  = useParams();
  const { isAuthenticated, getIdTokenClaims,user} = useAuth0();

  const [todos, setTodos] = useState(null);

  const deleteTodos = async(id: string) => {
    const accessToken = await getIdTokenClaims();
    await fetch(`${process.env.REACT_APP_SERVER_BASE_URL}/todos/delete/${id}`, {
      method: "delete",
      headers: new Headers({
        "Content-Type": "application/json",
        Accept: "application/json",
        "authorization": `Bearer ${accessToken.__raw}`
      })
    });
    _removeTodoFromView(id);
    history.push('/');
  }

  const _removeTodoFromView = (id: string) => {
    const index = todos.findIndex((todos: { _id: string; }) => todos._id === id);
    todos.splice(index, 1);  
  }

  useEffect(() => {
    const getAllTodos = async (): Promise<any> => {
      const response = await fetch(`${process.env.REACT_APP_SERVER_BASE_URL}/todos/todos`);
      const json = await response.json();
      setTodos(json)
    }
    getAllTodos();
  }, [TodosId])

    return (
        <section className="blog-area section">
        <div className="container">
          <div className="row">
            {   todos && todos.map((todos: { title: React.ReactNode; _id: any; description:any;}) => (  
              <div className="col-lg-4 col-md-6" key={todos._id}>
              <div className="card h-100">
                <div className="single-todo todo-style-1">
                  <div className="blog-info">

                    <h4 className="title">
                      <span>
                      <p>Title:  <b>{todos.title}</b></p>
                      <br />
                    <p> ID:  <b>{todos._id}</b></p>
                      </span>
                    </h4>
                  </div>
                </div>

                <ul className="todo-footer">
                  {
                    
                    isAuthenticated && 
                   <li>
                   <span>
              
                      <b><p>Your Todo Message :</p><br />{todos.description}</b>
                      </span>
                      </li>
                    }<br />
                  <br />
                
                  
                  {
                      isAuthenticated && 
                      <Link to={`/todos/update/${todos._id}`} className="btn btn-sm btn-outline-primary">Edit Todo</Link>
                  }
                     
                      
                    <br />
                    <br />
                  
                  
                    {
                      isAuthenticated &&
                      <button className="btn btn-sm btn-outline-secondary" onClick={() => deleteTodos(todos._id)}>Delete Todo</button>
                    }
                 
                </ul>
              </div>
            </div>
            ))}
          </div>
        </div>
      </section>
    );
}


export default Home;