import React from 'react';
import './App.css'
import { Switch, Route, Redirect } from 'react-router-dom';
import Navbar from './components/NavBar';
import Home from './components/Home';
import Edit from './components/todos/Edit';
import Create from './components/todos/Create';


function App(): JSX.Element {

  return (
    <div className="App">
    <Navbar />
      <div className={'container'}>
        <Switch>
          <Route exact path={"/"} >

            <h1 style={{color: "lightblue", marginTop: "30px"}}>Welcome</h1>
            
          <h1 style={{color: "red", marginTop: "70px"}}>Please Signup And Click on Home Button To See All the Todos</h1>
      
            </Route>
        <Route exact path={"/todos"}  component={Home} />
         
          <Route path={"/todos/update/:TodosId"} component={ Edit }/>
          <Route path={"/todos/create"} component={Create} />
        </Switch>

      
        

      </div>
      
    </div>
  );
}

export default App;
